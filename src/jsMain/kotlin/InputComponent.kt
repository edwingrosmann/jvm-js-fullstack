import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onSubmitFunction
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.Event
import react.RProps
import react.dom.form
import react.dom.input
import react.functionalComponent
import react.useState

external interface InputProps : RProps {
    var onSubmit: (String) -> Unit
    var wipe: Boolean
    var default: String
}

val InputComponent = functionalComponent<InputProps> { props ->
    val (text, setText) = useState(props.default)

    val submitHandler: (Event) -> Unit = {
        it.preventDefault()
        if (props.wipe) setText("")
        props.onSubmit(text)
    }

    val changeHandler: (Event) -> Unit = {
        val value = (it.target as HTMLInputElement).value
        setText(value)
    }

    form {
        attrs.onSubmitFunction = submitHandler
        input(InputType.text) {
            attrs.onChangeFunction = changeHandler
            attrs.value = text
        }
    }
}