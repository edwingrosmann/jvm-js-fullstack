import kotlinext.js.jsObject
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.html.js.onClickFunction
import mu.KotlinLogging
import react.*
import react.dom.*
import kotlin.js.Date

private val scope = MainScope()
var organisation = "Waitakere"
var groupName: String = "10"
var lastMessage: String = ""
val logger = KotlinLogging.logger {}

val App = functionalComponent<RProps> { _ ->
    val (chat, setChat) = useState(emptyList<ChatItem>())
    val (gpxList, setGpxList) = useState(emptyList<GpxData>())
    val (message, setMessage) = useState(Response())

    useEffect(dependencies = listOf()) {
        val x = scope.launch {
            setChat(getChat())
            setGpxList(getGpxList())
            setMessage(LastItemScanner.getLastItem())
        }
        logger.info { "Coroutine ${x}" }
    }

    h3 {

        scope.launch {
            setMessage(LastItemScanner.getLastItem())
            if (message.item != lastMessage) {
                lastMessage = message.item
                setChat(getChat())
            }
            delay(2500)
        }
        +"Most Recent: ${message.item}"


    }

    p {
        +"Organisation and Group: "
        child(
            InputComponent,
            props = jsObject {
                wipe = false
                default = organisation
                onSubmit = { t ->
                    organisation = t
                    scope.launch {
                        setChat(getChat())
                    }
                }
            }
        )
        child(
            InputComponent,
            props = jsObject {
                wipe = false
                default = groupName
                onSubmit = { t ->
                    groupName = t
                    scope.launch {
                        setChat(getChat())
                    }
                }
            }
        )

    }
    ul {
        chat.sortedByDescending { it.date }.forEach { item ->
            li {
                attrs.onClickFunction = {
                    scope.launch {
                        deleteChatItem(item)
                        setChat(getChat())
                    }
                }
                +"$item"
            }
        }
    }
    p {
        +"New Chat Item:"
        child(
            InputComponent,
            props = jsObject {
                wipe = true
                onSubmit = { input ->
                    scope.launch {
                        addChatItem(
                            ChatItem(
                                desc = input.replace("!", ""),
                                priority = input.count { it == '!' },
                                group = Group(organisation, groupName),
                                date = Date.now()
                            )
                        )
                        setChat(getChat())
                    }
                }
            }
        )
    }
    ul {
        gpxList.iterator().forEach { item ->
            li {
                +"${item.id}. "
                br {}
                small { i { +"${item.analysis}" } }
            }
        }
    }
}

