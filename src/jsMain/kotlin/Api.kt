import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.browser.window


val endpoint = window.location.origin // only needed until https://github.com/ktorio/ktor/issues/1695 is resolved

val jsonClient = HttpClient {
    install(JsonFeature) {
        serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
            isLenient = true
            ignoreUnknownKeys = true
            allowSpecialFloatingPointValues = true
            useArrayPolymorphism = false
        })
    }
}

suspend fun getChat(): List<ChatItem> {
    return jsonClient.get<List<ChatItem>>(endpoint + ChatItem.endpoints.all + orgFilter() + groupFilter())
}

private fun orgFilter() = if (organisation.isNotBlank()) "?o=$organisation" else ""
private fun groupFilter() = if (organisation.isNotBlank() && groupName.isNotBlank()) "&n=$groupName" else ""

suspend fun getGpxList(): List<GpxData> {
    return jsonClient.get(endpoint + GpxData.path)
}

suspend fun addChatItem(chatItem: ChatItem) {
    jsonClient.post<Unit>(endpoint + ChatItem.endpoints.all) {
        contentType(ContentType.Application.Json)
        logger.info { chatItem }
        body = chatItem
    }
}

suspend fun deleteChatItem(chatItem: ChatItem) {
    jsonClient.delete<Unit>(endpoint + ChatItem.endpoints.all + "/${chatItem._id.encodeURLParameter(false)}")
}



