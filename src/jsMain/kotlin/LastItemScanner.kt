import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

data class Response(val item: String = "Wait for it...")


///Create A Scanner Singleton...
object LastItemScanner {
    init {
        scan()
    }

    private var lastItem = Response()

    private val jsonClient = HttpClient {
        install(JsonFeature) { serializer = KotlinxSerializer() }
    }

    private fun scan() {
        MainScope().launch {
            while (true) {
                lastItem = Response(
                    jsonClient.get<MostRecentData>(endpoint + ChatItem.endpoints.last + "?o=$organisation&n=$groupName")
                        .toString()
                )
                delay(5000)
            }
        }
    }

    fun getLastItem() = lastItem.copy()
}