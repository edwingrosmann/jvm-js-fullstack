import kotlin.test.Test
import kotlin.test.assertEquals
//import org.litote.kmongo.json

class StringHasherTest {


    @Test
    fun testHash() {

        assertEquals("N/A", "".md5())
        assertEquals("0cbc6611f5540bd0809a388dc95a615b", "Test".md5())
        assertEquals("532eaabd9574880dbf76b9b8cc00832c20a6ec113d682299550d7a6e0f345e25", "Test".sha256())
        assertEquals("098f6bcd4621d373cade4e832627b4f6", "test".md5())
        assertEquals("9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08", "test".sha256())
    }


    @Test
    fun chatItemTest(){
        assertEquals("Anonymous from  Public/Anonymous says 'What What?' (md5=2c64362a762349bf7c50506e46688518)", ChatItem().toString())
        assertEquals("Anonymous from  Public/Anonymous says '' (md5=N/A)", ChatItem(desc= "").toString())
        assertEquals("""cbddac4eccfd5b376d8498b478589a75""", ChatItem(desc="Edwin").hash)
        assertEquals("""59ccf8974368cab72e8e0ac969872f6e""", ChatItem("Henny","Cat Lady",1,1234.56789).hash)
    }
}
