import com.mongodb.ConnectionString
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.Filters.gt
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.pipeline.*
import mu.KotlinLogging
import org.bson.conversions.Bson
import org.litote.kmongo.EMPTY_BSON
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.descending
import org.litote.kmongo.eq
import org.litote.kmongo.reactivestreams.KMongo

const val NULL: String = "null"

val connectionString: ConnectionString? = System.getenv("MONGODB_URI")?.let {
    ConnectionString("$it?retryWrites=false")
}

val client =
    if (connectionString != null) KMongo.createClient(connectionString).coroutine else KMongo.createClient().coroutine
val database = client.getDatabase(connectionString?.database ?: "test")
val itemsCollection = database.getCollection<ChatItem>()
val userLocationCollection = database.getCollection<UserLocation>()
val gpx_data = database.getCollection<GpxData>("gpx_data")

val logger = KotlinLogging.logger {}


fun main() {

    val port = System.getenv("PORT")?.toInt() ?: 9090

    embeddedServer(Netty, port) {
        install(ContentNegotiation) {
            json()
        }
        install(CORS) {
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Delete)
            anyHost()
        }
        install(Compression) {
            gzip()
        }

        routing {
            get("/") {
                call.respondText(
                    this::class.java.classLoader.getResource("index.html")!!.readText(),
                    ContentType.Text.Html
                )
            }
            static("/") {
                resources("")
            }
            route(ChatItem.endpoints.all) {
                get {
                    val group = group()
                    call.respond(
                        itemsCollection.find(
                            organisationNameFilter(group),
                            groupNameFilter(group),
                            targetTypeFilter(targetType())
                        ).toList(),
                    )
                }
                post {
                    val item = call.receive<ChatItem>()

                    //The _id may NOT have trailing spaces: the rest-api cannot handle those in the http-call..
                    item._id = item._id.trim()

                    if (itemsCollection.findOneById(item._id) == null) {
                        itemsCollection.insertOne(item)

                    } else {
                        itemsCollection.updateOneById(id = item._id, update = item)
                    }
                    logger.info("Received Item $item")
                    call.respond(HttpStatusCode.OK)
                }
                delete("/{id}") {

                    //The Id is URL-Encoded; where spaces are plus-signs...
                    try {
                        val id = call.parameters["id"].toString().decodeURLQueryComponent(plusIsSpace = true)
                        itemsCollection.deleteOne(ChatItem::_id eq id)
                        call.respond(HttpStatusCode.OK)
                    } catch (e: Exception) {
                        call.respond(HttpStatusCode.NotAcceptable)
                    }
                }
            }

            route(ChatItem.endpoints.fromDate) {
                get("/{date}") {
                    val fromDate =
                        call.parameters["date"].toString().decodeURLQueryComponent(plusIsSpace = true).toLong()
                    val items = itemsCollection
                        .find(
                            gt("date", fromDate),
                            groupFilter(group()),
                            targetTypeFilter(TargetType.GROUP),
                        )
                        .toList()
                    call.respond(items)
                }
            }
            route(ChatItem.endpoints.last) {
                get {
                    val group = group()
                    call.respond(
                        MostRecentData(
                            itemsCollection.find(
                                groupFilter(group),
                                targetTypeFilter(TargetType.GROUP)
                            )
                                .sort(descending(ChatItem::date)).limit(1).first() ?: ChatItem(),
                            itemsCollection.find(
                                groupFilter(group),
                                targetTypeFilter(TargetType.INDIVIDUAL),
                            )
                                .sort(descending(ChatItem::date)).limit(1).first() ?: ChatItem()
                        )
                    )
                }
            }

            route(UserLocation.endpoints.postLocation) {
                post {
                    val item = call.receive<UserLocation>()

                    if (userLocationCollection.findOneById(item._id) == null) {
                        userLocationCollection.insertOne(item)

                    } else {
                        userLocationCollection.updateOneById(id = item._id, update = item)
                    }
                    logger.info("Received User Location $item")
                    call.respond(HttpStatusCode.OK)
                }
            }


            route(UserLocation.endpoints.allUserLocations) {
                get {
                    call.respond(userLocationCollection.find(groupFilter(group())).toList())
                }
            }

            route(GpxData.path) {
                get {
                    call.respond(gpx_data.find().toList())
                }
            }
        }
    }.start(wait = true)
}


fun targetTypeFilter(targetType: TargetType?): Bson {
    return targetType?.let { eq("target.type", it) } ?: EMPTY_BSON
}

fun organisationNameFilter(group: Group): Bson {
    return if (group.organisation == NULL) EMPTY_BSON else eq("group.organisation", group.organisation)
}

fun groupNameFilter(group: Group): Bson {
    return if (group.name == NULL) EMPTY_BSON else eq("group.name", group.name)
}

fun groupFilter(group: Group): Bson {
    return eq("group", group)
}


private fun PipelineContext<Unit, ApplicationCall>.group(): Group {
    val group = Group(
        call.parameters["o"].toString().decodeURLQueryComponent(plusIsSpace = true),
        call.parameters["n"].toString().decodeURLQueryComponent(plusIsSpace = true)
    )
    println("GROUP=$group")
    return group
}

private fun PipelineContext<Unit, ApplicationCall>.targetType(): TargetType? {
    try {
        return TargetType.valueOf(call.parameters["tt"].toString().decodeURLQueryComponent(plusIsSpace = true).trim())
    } catch (e: IllegalArgumentException) {
        return null
    }
}

