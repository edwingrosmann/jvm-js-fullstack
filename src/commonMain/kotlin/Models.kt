import io.ktor.http.*
import io.ktor.util.date.*
import kotlinx.serialization.Serializable

const val ANONYMOUS: String = "Anonymous"
const val PUBLIC: String = "Public"
const val EVERYBODY: String = "Everybody"

@Serializable
enum class TargetType { GROUP, INDIVIDUAL }

@Serializable
data class Group(val organisation: String, val name: String? = null) {

    override fun hashCode(): Int {
        return organisation.hashCode()+(name?.hashCode()?:0)
    }
    override fun equals(other: Any?): Boolean {
        return other?.let { return organisation == (it as Group).organisation && (it.name.isNullOrBlank() || name == it.name)}
            ?: false
    }
}

@Serializable
data class Target(val type: TargetType = TargetType.GROUP, val individualName: String? = null)

@Serializable
data class ChatItem(
    val userName: String = ANONYMOUS,
    val desc: String = "What What?",
    val priority: Int = 0,
    val date: Double = 0.0,
    val group: Group = Group(PUBLIC, ANONYMOUS),
    val target: Target = Target(TargetType.GROUP, EVERYBODY),
) {

    var _id: String = ""
    var hash: String = ""

    //Funny story: the md5() function can ONLY successfully be executed from the init-block;
    // so NOT at the declaration statement above; Ignore the linter :-)
    init {
        _id = "_${userName.trim()}-${date.toLong()}"
        hash = desc.md5()
    }

    companion object {
        val endpoints = Endpoints("/chatList", "/chatListFromDate", "/lastItem")
    }

    override fun toString(): String {
        return "$userName from  ${group.organisation}/${group.name} says '$desc' (md5=$hash)"
    }
}

@Serializable
data class MostRecentData(val lastItem: ChatItem, val lastAlert: ChatItem)

data class Endpoints(val all: String, val fromDate: String, val last: String)

@Serializable
data class GpxData(val _id: String, val analysis: Analysis) {
    val id = _id

    companion object {
        const val path = "/gpxData"
    }

    override fun toString(): String {
        return "${id}.   ${Typography.nbsp}${analysis} "
    }
}

@Serializable
data class Analysis(
    val key: String,
    val start_time: String,
    val stop_time: String,
    val total_time: String,
    val total_moving_time: String,
    val average_speed_kph: Double,
    val average_moving_speed_kph: Double,
    val average_pace_mpk: Double,
    val average_moving_pace_mpk: Double,
    val distance: Double,
    val distance_flat: Double,
    val distance_ascending: Double,
    val distance_descending: Double,
    val total_elevate_up: Double,
    val total_elevate_down: Double,
    val highest_elevation: Double,
    val lowest_elevation: Double,
    val elevation_difference: Double
) {

    override fun toString(): String {
        return "$key; Start Time: $start_time, Stop Time: $stop_time, Distance: ${((distance/10).toInt())/100.0}[km], Moving Speed: $average_moving_speed_kph"
    }
}

@Serializable
data class UserLocation(
    val userName: String,
    val group: Group,
    val lon: Double,
    val lat: Double,
    val time: Long,
    val speed: Double,
    val altitude: Double,
    val accuracy: Double
) {
    val _id: String = userName.trim()

    companion object {
        val endpoints = Endpoints("/userLocation", "/userLocation", "/allUserLocations")
    }

    data class Endpoints(val postLocation: String, val lastLocation: String, val allUserLocations: String)

    override fun toString(): String {
        return "user=$userName, lon=$lon, lat=$lat, time=${GMTDate(time).toHttpDate()}, speed=${speed * 3.6}km/h, altitude=${altitude}m, accuracy=${accuracy}m"
    }
}

