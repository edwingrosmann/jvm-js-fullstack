import com.soywiz.krypto.md5
import com.soywiz.krypto.sha256
import io.ktor.utils.io.core.*

fun String.md5(): String = if (this.isBlank()) "N/A" else this.toByteArray().md5().hexLower
fun String.sha256(): String = if (this.isBlank()) "N/A" else this.toByteArray().sha256().hexLower
